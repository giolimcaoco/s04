"""
Create a folder called activity and inside the activity, a file called activity.py

Create the application to solve the following problems:

Specifications:
Specification
1. Create an abstract class called Animal that has the following abstract methods
Abstract Methods: 
* eat(food)
* make_sound()

2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:

Properties:
* name
* breed
* age

Methods:
* getters and setters
* implementation of abstract methods
* call()

After completing the activity, push the activity solution to gitlab and paste the link to the repository in Boodle
"""

from abc import ABC, abstractclassmethod

class Animal_abc(ABC):
	@abstractclassmethod
	def eat(food):
		pass

	def make_sound(self):		
		pass

class Animal(Animal_abc):
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	# get
	def get_name(self):
		return self._name 

	def get_breed(self):
		return self._breed 

	def get_age(self):
		return (self._age)

	# set
	def set_name(self, name):
		self._name = _name

	def set_breed(self):
		self._breed = breed

	def set_age(self, age):
		self._age = age 

class Dog(Animal):
	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self._name}")

class Cat(Animal):
	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Miaow! Nyaw")

	def call(self):
		print(f"{self._name} come on!")

# Test Cases

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()




